package com.nlmk.evteev.tm.repository;

import com.nlmk.evteev.tm.entity.User;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Класс для работы с сущностью пользователя
 */
public class UserRepository {

    private final List<User> users = new ArrayList<>();

    public UserRepository() {
    }

    /**
     * Создание пользователя
     *
     * @param firstName  фамилия
     * @param middleName имя
     * @param secondName отчество
     * @param logonName  логон пользователя
     * @param password   пароль
     * @param asAdmin    признак администратора
     * @return созданный в системе пользователь {@link User}
     */
    public User create(final String firstName, final String secondName, final String middleName,
                       final String logonName, final String password, final Boolean asAdmin) {
        final User user = new User(firstName, secondName, middleName, logonName, password, asAdmin);
        users.add(user);
        return user;
    }

    /**
     * Создание пользователя
     *
     * @param logonName регистрационное имя
     * @param password  пароль
     * @param asAdmin   признак администратора
     * @return пользователь {@link User}
     */
    public User create(final String logonName, final String password, final Boolean asAdmin) {
        final User user = new User();
        user.setLogonName(logonName);
        user.setPassword(password);
        user.setUserRole(asAdmin ? User.UserRoles.ADMIN : User.UserRoles.USER);
        users.add(user);
        return user;
    }

    /**
     * Изменение пользователя
     *
     * @param firstName  фамилия
     * @param middleName имя
     * @param secondName отчество
     * @param logonName  логон пользователя
     * @param password   пароль
     * @param asAdmin    признак администратора
     * @return пользователь {@link User}
     */
    public User update(final String firstName, final String secondName, final String middleName,
                       final String logonName, final String password, final Boolean asAdmin) {
        User user = getUserByLogonName(logonName);
        if (user == null) return null;
        user.setFirstName(firstName);
        user.setSecondName(secondName);
        user.setMiddleName(middleName);
        user.setPassword(password);
        user.setUserRole(asAdmin ? User.UserRoles.ADMIN : User.UserRoles.USER);
        return user;
    }

    /**
     * Обновление пользователя
     *
     * @param user сущность пользователя для обновления
     * @return пользователь
     * @see User
     */
    public User update(final User user) {
        final int index = users.indexOf(user);
        if (index == -1) return null;
        users.set(index, user);
        return user;
    }

    /**
     * Возвращает пользователя по его логину
     *
     * @param logonName логин пользователя
     * @return пользователь {@link User} или NULL, если не найден
     */
    public User getUserByLogonName(final String logonName) {
        final List<User> filterUserList = users.stream().filter(user -> user.getLogonName().equals(logonName)).collect(Collectors.toList());
        if (filterUserList.size() == 0) return null;
        return filterUserList.get(0);
    }

    /**
     * Удаление пользователя
     *
     * @param logonName логин пользователя
     * @return удаленный пользователь {@link User}
     * или NULL, если такого не существует
     */
    public User deleteUser(final String logonName) {
        User user = getUserByLogonName(logonName);
        if (user == null) return null;
        users.remove(user);
        return user;
    }

    /**
     * Список пользователей
     *
     * @return список пользователей в системе {@link List}
     * @see User
     */
    public List<User> findAll() {
        return users;
    }

}
