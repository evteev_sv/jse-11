package com.nlmk.evteev.tm.constant;

/**
 * Класс содержит терминальные константы
 */
public class TerminalConst {

    public static final String CMD_HELP = "help";
    public static final String CMD_VERSION = "version";
    public static final String CMD_ABOUT = "about";
    public static final String CMD_EXIT = "exit";

    public static final String PROJECT_CREATE = "proj-create";
    public static final String PROJECT_CLEAR = "proj-clear";
    public static final String PROJECT_LIST = "proj-list";
    public static final String PROJECT_VIEW = "proj-view";
    public static final String PROJECT_VIEW_WITH_TASKS = "proj-view-with-tasks";
    public static final String PROJECT_REMOVE_BY_NAME = "proj-remove-by-name";
    public static final String PROJECT_REMOVE_BY_ID = "proj-remove-by-id";
    public static final String PROJECT_REMOVE_BY_INDEX = "proj-remove-by-index";
    public static final String PROJECT_UPDATE_BY_INDEX = "proj-update-by-index";
    public static final String PROJECT_UPDATE_BY_ID = "proj-update-by-id";
    public static final String PROJECT_ADD_TASK_BY_IDS = "proj-add-task-by-ids";
    public static final String PROJECT_REMOVE_TASK_BY_IDS = "proj-remove-task-by-ids";
    public static final String PROJECT_REMOVE_TASKS = "proj-remove-tasks";
    public static final String PROJECT_REMOVE_WITH_TASKS = "proj-remove-with-tasks";

    public static final String TASK_CREATE = "task-create";
    public static final String TASK_CLEAR = "task-clear";
    public static final String TASK_LIST = "task-list";
    public static final String TASK_VIEW = "task-view";
    public static final String TASK_VIEW_WITHOUT_PROJECT = "task-view-without-project";
    public static final String TASK_VIEW_BY_PROJECT = "task-view-by-project";
    public static final String TASK_REMOVE_BY_NAME = "task-remove-by-name";
    public static final String TASK_REMOVE_BY_ID = "task-remove-by-id";
    public static final String TASK_REMOVE_BY_INDEX = "task-remove-by-index";
    public static final String TASK_UPDATE_BY_INDEX = "task-update-by-index";
    public static final String TASK_UPDATE_BY_ID = "task-update-by-id";

    public static final String USER_CREATE = "user-create";
    public static final String USER_UPDATE = "user-update";
    public static final String USER_DELETE = "user-delete";
    public static final String USER_LIST = "user-list";

}
