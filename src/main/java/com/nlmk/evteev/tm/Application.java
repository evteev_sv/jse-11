package com.nlmk.evteev.tm;

import com.nlmk.evteev.tm.controller.ProjectTaskController;
import com.nlmk.evteev.tm.controller.SystemController;
import com.nlmk.evteev.tm.controller.UserController;

import java.util.Scanner;

import static com.nlmk.evteev.tm.constant.TerminalConst.*;

/**
 * Основной класс
 */
public class Application {

    private final SystemController systemController = new SystemController();
    private final ProjectTaskController projectTaskController = new ProjectTaskController();
    private final UserController userController = new UserController();

    {
        userController.createUser("Иванов", "Иванович", "Иван",
            "ivanov", "qwerty", true);
        userController.createUser("Петров", "Петрович", "Петр",
            "petrov", "asdzxc", false);
    }

    /**
     * Точка входа
     *
     * @param args дополнительные аргументы запуска
     */
    public static void main(String[] args) {
        final Scanner scanner = new Scanner(System.in);
        final Application application = new Application();
        application.systemController.displayWelcome();
        application.run(args);
        String command = "";

        while (!CMD_EXIT.equals(command)) {
            command = scanner.nextLine();
            application.run(command);
            System.out.println();
        }
    }

    /**
     * Основной метод исполнения
     *
     * @param args дополнительные параметры запуска
     */
    public void run(final String[] args) {
        if (args == null || args.length < 1) return;
        final String param = args[0];
        run(param);
    }

    /**
     * Обработка консольного ввода
     *
     * @param cmdString команда с консоли
     * @return код выполнения
     */
    public int run(final String cmdString) {
        if (cmdString == null || cmdString.isEmpty()) return -1;
        switch (cmdString) {
            case CMD_VERSION:
                return systemController.displayVersion();
            case CMD_HELP:
                return systemController.displayHelp();
            case CMD_ABOUT:
                return systemController.displayAbout();
            case CMD_EXIT:
                systemController.displayExit();

            case PROJECT_CREATE:
                return projectTaskController.getProjectController().createProject();
            case PROJECT_CLEAR:
                return projectTaskController.getProjectController().clearProject();
            case PROJECT_LIST:
                return projectTaskController.getProjectController().listProject();
            case PROJECT_VIEW:
                return projectTaskController.getProjectController().viewProjectByIndex();
            case PROJECT_VIEW_WITH_TASKS:
                return projectTaskController.viewProjectWithTasks();
            case PROJECT_REMOVE_BY_ID:
                return projectTaskController.getProjectController().removeProjectById();
            case PROJECT_REMOVE_BY_NAME:
                return projectTaskController.getProjectController().removeProjectByName();
            case PROJECT_REMOVE_BY_INDEX:
                return projectTaskController.getProjectController().removeProjectByIndex();
            case PROJECT_UPDATE_BY_INDEX:
                return projectTaskController.getProjectController().updateProjectByIndex();
            case PROJECT_UPDATE_BY_ID:
                return projectTaskController.getProjectController().updateProjectById();
            case PROJECT_ADD_TASK_BY_IDS:
                return projectTaskController.addTaskToProjectByIds();
            case PROJECT_REMOVE_TASK_BY_IDS:
                return projectTaskController.removeTaskFromProjectById();
            case PROJECT_REMOVE_TASKS:
                return projectTaskController.clearProjectTasks();
            case PROJECT_REMOVE_WITH_TASKS:
                return projectTaskController.removeProjectWithTasks();

            case TASK_CREATE:
                return projectTaskController.getTaskController().createTask();
            case TASK_CLEAR:
                return projectTaskController.getTaskController().clearTask();
            case TASK_LIST:
                return projectTaskController.getTaskController().listTask();
            case TASK_VIEW:
                return projectTaskController.getTaskController().viewTaskByIndex();
            case TASK_VIEW_WITHOUT_PROJECT:
                return projectTaskController.getTaskController().findAllWithoutProject();
            case TASK_VIEW_BY_PROJECT:
                return projectTaskController.getTaskController().findAllByProjectId();
            case TASK_REMOVE_BY_ID:
                return projectTaskController.getTaskController().removeTaskById();
            case TASK_REMOVE_BY_NAME:
                return projectTaskController.getTaskController().removeTaskByName();
            case TASK_REMOVE_BY_INDEX:
                return projectTaskController.getTaskController().removeTaskByIndex();
            case TASK_UPDATE_BY_ID:
                return projectTaskController.getTaskController().updateTaskById();
            case TASK_UPDATE_BY_INDEX:
                return projectTaskController.getTaskController().updateTaskByIndex();

            case USER_CREATE:
                return userController.createUser();
            case USER_UPDATE:
                return userController.updateUser();
            case USER_DELETE:
                return userController.deleteUser();
            case USER_LIST:
                return userController.listUsers();

            default:
                return systemController.displayError();
        }
    }

}
