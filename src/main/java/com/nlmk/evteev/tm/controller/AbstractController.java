package com.nlmk.evteev.tm.controller;

import java.util.Scanner;

/**
 * Абстрактный класс контроллера
 */
public abstract class AbstractController {

    protected final Scanner scanner = new Scanner(System.in);

}
