package com.nlmk.evteev.tm.controller;

import com.nlmk.evteev.tm.entity.Project;
import com.nlmk.evteev.tm.service.ProjectService;

/**
 * Класс контроллера проектов
 */
public class ProjectController extends AbstractController {

    private final ProjectService projectService;

    /**
     * Конструктор контроллера
     *
     * @param projectService класс сервииса проектов {@link ProjectService}
     */
    protected ProjectController(ProjectService projectService) {
        this.projectService = projectService;
    }

    /**
     * Удаление проекта по имени
     *
     * @return код исполнения
     */
    public int removeProjectByName() {
        System.out.println("[Remove project by name]");
        System.out.println("Введите имя проекта: ");
        final String name = scanner.nextLine();
        Project project = projectService.removeByName(name);
        if (project == null) {
            System.out.println("[FAIL]");
        } else {
            System.out.println("[OK]");
        }
        return 0;
    }

    /**
     * Удаление проекта по коду
     *
     * @return код исполнения
     */
    public int removeProjectById() {
        System.out.println("[Remove project by id]");
        System.out.println("Введите ID проекта: ");
        final Long vId = scanner.nextLong();
        Project project = projectService.removeById(vId);
        if (project == null) {
            System.out.println("[FAIL]");
        } else {
            System.out.println("[OK]");
        }
        return 0;
    }

    /**
     * Удаление проекта по индексу
     *
     * @return код исполнения
     */
    public int removeProjectByIndex() {
        System.out.println("[Remove project by index]");
        System.out.println("Введите индекс проекта: ");
        final int vId = scanner.nextInt();
        Project project = projectService.removeByIndex(vId);
        if (project == null) {
            System.out.println("[FAIL]");
        } else {
            System.out.println("[OK]");
        }
        return 0;
    }

    /**
     * Изменение проекта по коду
     *
     * @return код исполнения
     */
    public int updateProjectById() {
        System.out.println("[Update project by id]");
        System.out.println("Введите код проекта: ");
        final Long vId = Long.getLong(scanner.nextLine());
        System.out.println("Введите новое имя проекта: ");
        final String name = scanner.nextLine();
        System.out.println("Введите новое описание проекта: ");
        final String description = scanner.nextLine();
        if (projectService.update(vId, name, description) != null) {
            System.out.println("[OK]");
        } else {
            System.out.println("[FAIL]");
        }
        return 0;
    }

    /**
     * Изменение проекта по индексу
     *
     * @return код исполнения
     */
    public int updateProjectByIndex() {
        System.out.println("[Update project by index]");
        System.out.println("Введите индекс проекта: ");
        final int vId = Integer.parseInt(scanner.nextLine()) - 1;
        final Project project = projectService.findByIndex(vId);
        if (project == null) {
            System.out.println("[FAIL]");
            return 0;
        }
        System.out.println("Введите новое имя проекта: ");
        final String name = scanner.nextLine();
        System.out.println("Введите новое описание проекта: ");
        final String description = scanner.nextLine();
        if (projectService.update(project.getId(), name, description) != null) {
            System.out.println("[OK]");
        } else {
            System.out.println("[FAIL]");
        }
        return 0;
    }

    /**
     * Просмотр проекта по индексу
     *
     * @return код исполнения
     */
    public int viewProjectByIndex() {
        System.out.println("Введите индекс проекта: ");
        final int index = scanner.nextInt() - 1;
        final Project project = projectService.findByIndex(index);
        viewProject(project);
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Просмотр проекта в консоли
     *
     * @param project проект {@link com.nlmk.evteev.tm.entity.Project}
     */
    public int viewProject(final Project project) {
        if (project == null) return 0;
        System.out.println("[View Project]");
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Поиск проекта по коду
     *
     * @param projectId код проекта
     * @return проект {@link Project}
     */
    public Project findProjectById(final Long projectId) {
        if (projectId == null) return null;
        return projectService.findById(projectId);
    }

    /**
     * Создание проекта
     *
     * @return код выполнения
     */
    public int createProject() {
        System.out.println("[CREATE PROJECT]");
        System.out.println("Укажите имя проекта: ");
        final String lv_name = scanner.nextLine();
        projectService.create(lv_name);
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Очистка проекта
     *
     * @return код выполнения
     */
    public int clearProject() {
        System.out.println("[CLEAR PROJECT]");
        projectService.clear();
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Список проектов
     *
     * @return код выполнения
     */
    public int listProject() {
        System.out.println("[LIST PROJECT]");
        System.out.println(projectService.findAll());
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Добавление задачи к проекту
     *
     * @param projectId код проекта
     * @param taskId    код задачи
     * @return код исполнения
     */
    public int addTaskToProject(final Long projectId, final Long taskId) {
        if (projectId == null) return 0;
        if (taskId == null) return 0;
        Project project = projectService.findById(projectId);
        if (project == null) return 0;
        project.getTasks().add(taskId);
        return 0;
    }

    /**
     * Удаление задачи из проекта
     *
     * @param projectId код проекта
     * @param taskId    код задачи
     * @return код исполнения
     */
    public int removeTaskFromProject(final Long projectId, final Long taskId) {
        if (projectId == null) return 0;
        if (taskId == null) return 0;
        Project project = projectService.findById(projectId);
        if (project == null) return 0;
        project.getTasks().remove(taskId);
        return 0;
    }

}
