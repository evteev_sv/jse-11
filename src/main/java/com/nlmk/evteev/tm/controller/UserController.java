package com.nlmk.evteev.tm.controller;

import com.nlmk.evteev.tm.entity.User;
import com.nlmk.evteev.tm.repository.UserRepository;
import com.nlmk.evteev.tm.service.UserService;

import java.util.List;

/**
 * Класс контроллера для работы с пользователем
 */
public class UserController extends AbstractController {

    private final UserRepository userRepository = new UserRepository();
    private final UserService userService = new UserService(userRepository);

    public UserController() {
    }

    /**
     * Создание пользователя
     *
     * @param firstName  фамилия
     * @param secondName отчество
     * @param middleName имя
     * @param logonName  логон пользователя
     * @param password   пароль
     * @param asAdmin    признак администратора
     * @return код исполнения
     */
    public int createUser(String firstName, String secondName, String middleName, String logonName, String password, Boolean asAdmin) {
        userService.create(firstName, secondName, middleName, logonName, password, asAdmin);
        return 0;
    }

    /**
     * Создание пользователя в интерактивном режиме
     *
     * @return код исполнения
     */
    public int createUser() {
        System.out.println("[CREATE USER]");
        System.out.println("Введите регистрационное имя:");
        final String logonName = scanner.nextLine();
        if (logonName == null || logonName.isEmpty()) {
            System.out.println("Регистрационное имя не может быть пустым!");
            System.out.println("[FAIL]");
            return 0;
        }
        if (userRepository.getUserByLogonName(logonName) != null) {
            System.out.println("Пользователь с таким регистрационным именем уже существует!");
            System.out.println("[FAIL]");
            return 0;
        }
        System.out.println("Введите пароль:");
        final String password = scanner.nextLine();
        if (password == null || password.isEmpty()) {
            System.out.println("Пароль не может быть пустым!");
            System.out.println("[FAIL]");
            return 0;
        }
        System.out.println("Предоставить административные права? (y - да, n - нет):");
        final String s = scanner.nextLine();
        boolean asAdmin;
        if (s == null || s.isEmpty() || s.trim().equals("n"))
            asAdmin = false;
        if (s.trim().equals("y"))
            asAdmin = true;
        else
            asAdmin = false;
        userService.create(logonName, password, asAdmin);
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Изменение пользователя
     *
     * @return код исполнения
     */
    public int updateUser() {
        System.out.println("[UPDATE USER DATA]");
        System.out.println("Введите регистрационное имя пользователя:");
        final String logonName = scanner.nextLine();
        User user = userService.getUserByLogonName(logonName);
        if (user == null) {
            System.out.println("Пользователя с таким регистрационным именем не существует.");
            System.out.println("[FAIL]");
            return 0;
        }
        System.out.println("Введите фамилию пользователя:");
        String firstName = scanner.nextLine();
        if (firstName != null && !firstName.isEmpty() && !firstName.equals(user.getFirstName()))
            user.setFirstName(firstName);
        System.out.println("Введите имя пользователя:");
        String middleName = scanner.nextLine();
        if (middleName != null && !middleName.isEmpty() && !middleName.equals(user.getMiddleName()))
            user.setMiddleName(middleName);
        System.out.println("Введите отчество пользователя:");
        String secondName = scanner.nextLine();
        if (secondName != null && !secondName.isEmpty() && !secondName.equals(user.getSecondName()))
            user.setSecondName(secondName);
        System.out.println("Введите новый пароль пользователя:");
        String password = scanner.nextLine();
        if (password == null || password.isEmpty()) {
            System.out.println("Пароль не может быть пустым!");
            System.out.println("[FAIL]");
            return 0;
        }
        user.setPassword(password);
        boolean asAdmin;
        System.out.println("Предоставить административные права? (y - да, n - нет):");
        final String s = scanner.nextLine();
        if (s == null || s.isEmpty() || s.trim().equals("n"))
            asAdmin = false;
        if (s.trim().equals("y"))
            asAdmin = true;
        else
            asAdmin = false;
        user.setUserRole(asAdmin ? User.UserRoles.ADMIN : User.UserRoles.USER);
        userService.update(user);
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Удаление пользователя
     *
     * @return код исполнения
     */
    public int deleteUser() {
        System.out.println("[DELETE USER]");
        System.out.println("Введите регистрационное имя пользователя:");
        final String logonName = scanner.nextLine();
        userService.deleteUser(logonName);
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Список пользователей
     *
     * @return список пользователей в системе {@link List}
     * @see User
     */
    public int listUsers() {
        System.out.println("[LIST USERS]");
        userRepository.findAll().forEach(user -> System.out.println(user));
        System.out.println("[OK]");
        return 0;
    }

}
