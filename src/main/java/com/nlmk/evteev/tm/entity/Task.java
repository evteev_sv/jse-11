package com.nlmk.evteev.tm.entity;

/**
 * Класс, описывающий задачи
 */
public class Task {

    private Long id = System.nanoTime();
    private String name = "";
    private String description = "";
    private Long projectId;

    /**
     * Конструктор по умолчанию
     */
    public Task() {
    }

    /**
     * Конструктор класса
     *
     * @param pName название задачи
     */
    public Task(String pName) {
        name = pName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    @Override
    public String toString() {
        return "Task {id = " + id + ", name = " + name + ", projectId = " + projectId + "}";
    }

}
