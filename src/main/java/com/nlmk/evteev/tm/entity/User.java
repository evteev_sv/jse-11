package com.nlmk.evteev.tm.entity;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Класс, описывающий пользователя
 */
public class User {

    private String firstName;
    private String secondName;
    private String middleName;
    private String logonName;
    private String password;
    private UserRoles userRole = UserRoles.USER;

    public User() {
    }

    /**
     * Конструктор
     *
     * @param firstName  фамилия
     * @param secondName отчество
     * @param middleName имя
     * @param logonName  регистрационное имя
     * @param password   пароль
     * @param asAdmin    признак администратора
     */
    public User(String firstName, String secondName, String middleName, String logonName,
                String password, Boolean asAdmin) {
        this.firstName = firstName;
        this.secondName = secondName;
        this.middleName = middleName;
        this.logonName = logonName;
        this.password = getMD5Hash(password);
        this.userRole = asAdmin ? UserRoles.ADMIN : UserRoles.USER;
    }

    /**
     * Получение hash пароля
     *
     * @param str пароль
     * @return сроковое представление hash
     */
    private static String getMD5Hash(final String str) {
        if (str == null || str.isEmpty()) return "";
        MessageDigest md;
        try {
            md = MessageDigest.getInstance("MD5");
            md.update(str.getBytes());
            return new String(md.digest());
        }
        catch (NoSuchAlgorithmException e) {
            return "";
        }
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLogonName() {
        return logonName;
    }

    public void setLogonName(String logonName) {
        this.logonName = logonName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = getMD5Hash(password);
    }

    public UserRoles getUserRole() {
        return userRole;
    }

    public void setUserRole(UserRoles userRole) {
        this.userRole = userRole;
    }

    public boolean isAdmin() {
        return userRole == UserRoles.ADMIN;
    }

    @Override
    public String toString() {
        return String.format("user: [firstName = %s,\n middleName = %s,\n secondName = %s,\nlogonName = %s, " +
                "\n isAdmin = %b, \n password = %h]",
            firstName, middleName, secondName, logonName, isAdmin(), password);
    }

    /**
     * Перечисление ролей
     */
    public enum UserRoles {
        USER,
        ADMIN
    }

}
