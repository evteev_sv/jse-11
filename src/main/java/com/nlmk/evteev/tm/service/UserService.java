package com.nlmk.evteev.tm.service;

import com.nlmk.evteev.tm.entity.User;
import com.nlmk.evteev.tm.repository.UserRepository;

import java.util.List;

/**
 * Сервисный класс для работы с репозиторием пользователя
 */
public class UserService {

    private UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    /**
     * Создание пользователя
     *
     * @param firstName  фамилия
     * @param secondName отчество
     * @param middleName имя
     * @param logonName  логон пользователя
     * @param password   пароль
     * @param asAdmin    признак администратора
     * @return созданный в системе пользователь {@link User}
     */
    public User create(String firstName, String secondName, String middleName, String logonName,
                       String password, Boolean asAdmin) {
        if (checkUserCredentials(firstName, secondName, middleName, logonName, password, asAdmin)) return null;
        return userRepository.create(firstName, secondName, middleName, logonName, password, asAdmin);
    }

    public User create(String logonName, String password, Boolean asAdmin) {
        if (logonName == null || logonName.isEmpty()) return null;
        if (password == null || password.isEmpty()) return null;
        if (asAdmin == null) asAdmin = false;
        return userRepository.create(logonName, password, asAdmin);
    }

    /**
     * Изменение пользователя
     *
     * @param firstName  фамилия
     * @param secondName отчество
     * @param middleName имя
     * @param logonName  логон пользователя
     * @param password   пароль
     * @param asAdmin    признак администратора
     * @return пользователь {@link User}
     */
    public User update(String firstName, String secondName, String middleName, String logonName,
                       String password, Boolean asAdmin) {
        if (checkUserCredentials(firstName, secondName, middleName, logonName, password, asAdmin)) return null;
        return userRepository.update(firstName, secondName, middleName, logonName, password, asAdmin);
    }

    /**
     * Обновление пользователя
     *
     * @param user сущность пользователя для обновления
     * @return пользователь
     * @see User
     */
    public User update(User user) {
        if (user == null) return null;
        return userRepository.update(user);
    }

    /**
     * Удаление пользователя
     *
     * @param logonName логин пользователя
     * @return удаленный пользователь {@link User}
     * или NULL, если такого не существует
     */
    public User deleteUser(String logonName) {
        if (logonName == null || logonName.isEmpty()) return null;
        return userRepository.deleteUser(logonName);
    }

    /**
     * Проверка вводимых данных
     *
     * @param firstName  фамилия
     * @param secondName отчество
     * @param middleName имя
     * @param logonName  логон пользователя
     * @param password   пароль
     * @param asAdmin    признак администратора
     * @return истина, если данные корректны, иначе ложь
     */
    private boolean checkUserCredentials(String firstName, String secondName, String middleName,
                                         String logonName, String password, Boolean asAdmin) {
        if (firstName == null || firstName.isEmpty()) return true;
        if (middleName == null || middleName.isEmpty()) return true;
        if (secondName == null || secondName.isEmpty()) return true;
        if (logonName == null || logonName.isEmpty()) return true;
        if (password == null || password.isEmpty()) return true;
        return asAdmin == null;
    }

    /**
     * Возвращает пользователя по его логину
     *
     * @param logonName логин пользователя
     * @return пользователь {@link User} или NULL, если не найден
     */
    public User getUserByLogonName(String logonName) {
        if (logonName == null || logonName.isEmpty()) return null;
        return userRepository.getUserByLogonName(logonName);
    }

    /**
     * Список пользователей
     *
     * @return список пользователей в системе {@link List}
     * @see User
     */
    public List<User> findAll() {
        return userRepository.findAll();
    }

}
